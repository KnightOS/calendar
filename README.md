A user-space application for KnightOS that shows a calendar. Note that the application is still under development.

For more about KnightOS, see [knightos.org](http://knightos.org) or [its GitHub page](https://github.com/KnightOS).

Screenshots
-----------
![Screenshot of the application](https://cdn.mediacru.sh/hhDpQDpQQNI5.png)

![Another screenshot](https://cdn.mediacru.sh/E1W6gZlg9P61.png)

Planned features
----------------
* Support for storing and organizing appointments.
